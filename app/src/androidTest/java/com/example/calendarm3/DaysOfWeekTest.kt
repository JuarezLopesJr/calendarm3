package com.example.calendarm3

import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.test.platform.app.InstrumentationRegistry
import com.example.calendarm3.ui.screens.calendar.DaysOfWeek
import com.example.calendarm3.utils.Tags.TAG_DAYS_OF_WEEK
import org.junit.Rule
import org.junit.Test

class DaysOfWeekTest {
    @get:Rule
    val composeRule = createComposeRule()

    private fun getTestStringArray(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.resources.getStringArray(id)

    @Test
    fun assert_Days_Of_Week_Displayed() {
        composeRule.setContent {
            DaysOfWeek()
        }

        getTestStringArray(R.array.days_of_week).forEachIndexed { index, day ->
            composeRule.onNodeWithTag(TAG_DAYS_OF_WEEK)
                .onChildAt(index).assertTextEquals(day)
        }
    }
}