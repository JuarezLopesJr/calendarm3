package com.example.calendarm3

import androidx.compose.foundation.layout.height
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsNotDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.unit.dp
import androidx.test.platform.app.InstrumentationRegistry
import com.example.calendarm3.utils.EventStack
import com.example.calendarm3.utils.Tags.TAG_EVENTS_STACK
import com.example.calendarm3.utils.TestDataFactory
import java.util.Calendar
import org.junit.Rule
import org.junit.Test

class EventsStackTest {
    @get:Rule
    val composeRule = createComposeRule()

    private val calendar = Calendar.getInstance()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Events_Stack_Displayed() {
        composeRule.setContent {
            EventStack(
                events = TestDataFactory.events(6, calendar),
                date = calendar
            )
        }

        composeRule.onNodeWithTag(TAG_EVENTS_STACK).assertIsDisplayed()
    }

    @Test
    fun assert_Events_Stack_Not_Displayed_When_No_Events_Exist() {
        composeRule.setContent {
            EventStack(
                events = emptyList(),
                date = calendar
            )
        }

        composeRule.onNodeWithTag(TAG_EVENTS_STACK).assertDoesNotExist()
    }

    @Test
    fun assert_View_More_Events_Displayed_When_Events_Exceed_Height() {
        composeRule.setContent {
            EventStack(
                modifier = Modifier.height(100.dp),
                events = TestDataFactory.events(10, calendar),
                date = calendar
            )
        }

        composeRule.onNodeWithContentDescription(getTestString(R.string.cd_view_more_events))
            .assertIsDisplayed()
    }

    @Test
    fun assert_View_More_Events_Not_Displayed_When_Events_Do_Not_Exceed_Height() {
        composeRule.setContent {
            EventStack(
                events = TestDataFactory.events(2, calendar),
                date = calendar
            )
        }

        composeRule.onNodeWithContentDescription(getTestString(R.string.cd_view_more_events))
            .assertIsNotDisplayed()
    }
}