package com.example.calendarm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.calendarm3.ui.screens.calendar.MonthHeader
import com.example.calendarm3.utils.TestDataFactory
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class HeaderTest {
    @get:Rule
    val composeRule = createComposeRule()

    private val title = TestDataFactory.randomString()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Test
    fun assert_Title_Displayed() {
        composeRule.setContent {
            MonthHeader(title = title, previousMonth = {}, nextMonth = {})
        }

        composeRule.onNodeWithText(title).assertIsDisplayed()
    }

    @Test
    fun assert_Previous_Month_Listener_Triggered() {
        val onPreviousClicked: () -> Unit = mock()

        composeRule.setContent {
            MonthHeader(title = title, previousMonth = onPreviousClicked, nextMonth = {})
        }

        composeRule
            .onNodeWithContentDescription(getTestString(R.string.cd_previous_month))
            .performClick()

        verify(onPreviousClicked).invoke()
    }

    @Test
    fun assert_Next_Month_Listener_Triggered() {
        val onNextClicked: () -> Unit = mock()

        composeRule.setContent {
            MonthHeader(title = title, previousMonth = {}, nextMonth = onNextClicked)
        }

        composeRule
            .onNodeWithContentDescription(getTestString(R.string.cd_next_month))
            .performClick()

        verify(onNextClicked).invoke()
    }
}