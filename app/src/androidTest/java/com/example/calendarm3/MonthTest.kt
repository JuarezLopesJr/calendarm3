package com.example.calendarm3

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import com.example.calendarm3.utils.MonthContent
import com.example.calendarm3.utils.Tags.TAG_MONTH
import com.example.calendarm3.utils.currentDateForCalendarPage
import java.util.Calendar
import org.junit.Rule
import org.junit.Test

class MonthTest {
    @get:Rule
    val composeRule = createComposeRule()

    private val calendar = currentDateForCalendarPage(Calendar.getInstance())

    @Test
    fun assert_Weeks_Of_Month_Displayed() {
        composeRule.setContent {
            MonthContent(date = calendar)
        }

        repeat(6) { position ->
            composeRule.onNodeWithTag(TAG_MONTH)
                .onChildAt(position)
                .assert(hasTestTag(calendar.get(Calendar.WEEK_OF_YEAR).toString()))
            calendar.add(Calendar.WEEK_OF_YEAR, 1)
        }
    }
}