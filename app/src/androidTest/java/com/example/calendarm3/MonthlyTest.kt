package com.example.calendarm3

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.test.platform.app.InstrumentationRegistry
import com.example.calendarm3.data.CalendarState
import com.example.calendarm3.ui.screens.calendar.MonthCalendar
import com.example.calendarm3.utils.Tags.TAG_HEADER
import com.example.calendarm3.utils.Tags.TAG_MONTH_PAGE
import com.google.accompanist.pager.ExperimentalPagerApi
import java.util.Calendar
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalPagerApi
@ExperimentalFoundationApi
@ExperimentalLifecycleComposeApi class MonthlyTest {
    @get:Rule
    val composeRule = createComposeRule()

    private val calendar = Calendar.getInstance()

    private fun getTestString(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(id)

    @Before
    fun setComposeTestRule() {
        composeRule.setContent {
            MonthCalendar(calendarState = CalendarState(date = calendar))
        }
    }

    @Test
    fun assert_Header_Displayed() {
        composeRule.onNodeWithTag(TAG_HEADER).assertIsDisplayed()
    }

    @Test
    fun assert_Month_Page_Displayed() {
        composeRule
            .onNodeWithTag(TAG_MONTH_PAGE + calendar.get(Calendar.MONTH).toString())
            .assertIsDisplayed()
    }

    @Test
    fun assert_Next_Month_Page_Displayed() {
        composeRule.onNodeWithContentDescription(getTestString(R.string.cd_next_month))
            .performClick()

        val nextMonth = (calendar.clone() as Calendar).apply {
            add(Calendar.MONTH, 1)
        }

        composeRule.onNodeWithTag(TAG_MONTH_PAGE + nextMonth.get(Calendar.MONTH))
            .assertIsDisplayed()
    }

    @Test
    fun assert_Previous_Month_Page_Displayed() {
        composeRule.onNodeWithContentDescription(getTestString(R.string.cd_previous_month))
            .performClick()

        val previousMonth = (calendar.clone() as Calendar).apply {
            add(Calendar.MONTH, -1)
        }

        composeRule.onNodeWithTag(TAG_MONTH_PAGE + previousMonth.get(Calendar.MONTH))
            .assertIsDisplayed()
    }
}