package com.example.calendarm3

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import com.example.calendarm3.ui.screens.calendar.WeekCalendar
import com.example.calendarm3.utils.Tags.TAG_EVENTS_STACK
import com.example.calendarm3.utils.Tags.TAG_WEEK_ROW
import com.example.calendarm3.utils.TestDataFactory
import java.util.Calendar
import org.junit.Rule
import org.junit.Test

class WeekTest {
    @get:Rule
    val composeRule = createComposeRule()
    private val calendar = Calendar.getInstance()

    @Test
    fun assert_Week_Displayed() {
        composeRule.setContent {
            WeekCalendar(calendarWeek = calendar, events = emptyList())
        }

        repeat(7) { position ->
            composeRule.onNodeWithTag(TAG_WEEK_ROW)
                .onChildAt(position)
                .assertTextEquals(calendar.get(Calendar.DAY_OF_MONTH).toString())
            calendar.add(Calendar.DAY_OF_YEAR, 1)
        }
    }

    @Test
    fun assert_Events_Displayed() {
        composeRule.setContent {
            WeekCalendar(
                calendarWeek = calendar,
                events = TestDataFactory.events(6, calendar)
            )
        }

        composeRule.onNodeWithTag(TAG_EVENTS_STACK).assertIsDisplayed()
    }
}