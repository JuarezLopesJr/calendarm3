package com.example.calendarm3.data

import androidx.compose.ui.graphics.Color
import java.util.Calendar

data class CalendarEvent(
    val color: Color,
    val startDate: Calendar,
    val endDate: Calendar
)