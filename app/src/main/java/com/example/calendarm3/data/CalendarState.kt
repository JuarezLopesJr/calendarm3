package com.example.calendarm3.data

import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import java.util.Calendar
import java.util.Locale

class CalendarState(val date: Calendar) {
    val pageCount: Int = Int.MAX_VALUE

    /* giving swipeable component the capacity to move forward and backward, between
     0 and Int.MAX_VALUE, it means that the start position we'll be at the middle of total */
    val startPosition: Int = Int.MAX_VALUE / 2

    var pagePosition by mutableStateOf(startPosition)

    /* calculated reference to the current month, displayed in the calendar header */
    val currentMonth: String by derivedStateOf {
        (date.clone() as Calendar).apply {
            add(Calendar.MONTH, pagePosition - startPosition)
        }.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, Locale.getDefault())
    }
}