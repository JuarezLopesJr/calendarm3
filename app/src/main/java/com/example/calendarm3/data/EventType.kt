package com.example.calendarm3.data

/* modelling what a day means in regards to an event */
enum class EventType {
    START, MIDDLE, END, SINGLE_DAY, NONE
}