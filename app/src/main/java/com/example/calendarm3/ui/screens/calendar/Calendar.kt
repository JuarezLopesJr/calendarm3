package com.example.calendarm3.ui.screens.calendar

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import com.example.calendarm3.data.CalendarState
import com.google.accompanist.pager.ExperimentalPagerApi
import java.util.Calendar

@ExperimentalLifecycleComposeApi
@ExperimentalPagerApi
@ExperimentalFoundationApi
@Composable
fun CalendarScreen(modifier: Modifier = Modifier) {
    val state = remember {
        CalendarState(Calendar.getInstance())
    }

    MonthCalendar(modifier = modifier.fillMaxSize(), calendarState = state)
}