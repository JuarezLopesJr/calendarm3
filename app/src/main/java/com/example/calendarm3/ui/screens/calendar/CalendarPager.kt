package com.example.calendarm3.ui.screens.calendar

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.LocalOverscrollConfiguration
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import java.util.Calendar

@ExperimentalFoundationApi
@ExperimentalPagerApi
@Composable
fun CalendarPager(
    modifier: Modifier = Modifier,
    pagerState: PagerState,
    startIndex: Int,
    date: Calendar,
    pageCount: Int
) {
    /* disabling overscroll effect */
    CompositionLocalProvider(LocalOverscrollConfiguration provides null) {
        HorizontalPager(
            modifier = modifier,
            count = pageCount,
            state = pagerState
        ) { index ->
            /* getting the numbers of months that has been swiped either way of the startIndex */
            val currentDate = (date.clone() as Calendar).apply {
                add(Calendar.MONTH, index - startIndex)
            }

            PagerMonth(currentDate = currentDate)
        }
    }
}