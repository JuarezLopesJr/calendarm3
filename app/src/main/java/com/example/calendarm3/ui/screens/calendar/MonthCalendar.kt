package com.example.calendarm3.ui.screens.calendar

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.calendarm3.R
import com.example.calendarm3.data.CalendarState
import com.example.calendarm3.utils.Tags.TAG_HEADER
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch

@ExperimentalLifecycleComposeApi
@ExperimentalFoundationApi
@ExperimentalPagerApi
@Composable
fun MonthCalendar(
    modifier: Modifier = Modifier,
    calendarState: CalendarState
) {
    val pagerState = rememberPagerState(initialPage = calendarState.startPosition)

    val scope = rememberCoroutineScope()

    snapshotFlow {
        pagerState.currentPage
    }.collectAsStateWithLifecycle(initialValue = calendarState.startPosition)
        .value.let { calendarState.pagePosition = it }

    Column(modifier = modifier) {
        MonthHeader(
            modifier = Modifier.fillMaxWidth(),
            title = calendarState.currentMonth,
            previousMonth = {
                scope.launch {
                    pagerState.animateScrollToPage(pagerState.currentPage - 1)
                }
            },
            nextMonth = {
                scope.launch {
                    pagerState.animateScrollToPage(pagerState.currentPage + 1)
                }
            }
        )

        Spacer(modifier = Modifier.height(24.dp))

        CalendarPager(
            modifier = Modifier.fillMaxSize(),
            pagerState = pagerState,
            startIndex = calendarState.startPosition,
            date = calendarState.date,
            pageCount = calendarState.pageCount
        )
    }
}

@Composable
fun MonthHeader(
    modifier: Modifier = Modifier,
    title: String,
    previousMonth: () -> Unit,
    nextMonth: () -> Unit
) {
    Row(
        modifier = modifier
            .testTag(TAG_HEADER)
            .padding(16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(onClick = previousMonth) {
            Icon(
                imageVector = Icons.Default.KeyboardArrowLeft,
                contentDescription = stringResource(id = R.string.cd_previous_month)
            )
        }

        Text(
            modifier = Modifier.weight(1f),
            text = title,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )

        IconButton(onClick = nextMonth) {
            Icon(
                imageVector = Icons.Default.KeyboardArrowRight,
                contentDescription = stringResource(id = R.string.cd_next_month)
            )
        }
    }
}