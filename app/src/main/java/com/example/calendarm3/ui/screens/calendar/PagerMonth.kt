package com.example.calendarm3.ui.screens.calendar

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.unit.dp
import com.example.calendarm3.utils.MonthContent
import com.example.calendarm3.utils.Tags.TAG_MONTH_PAGE
import com.example.calendarm3.utils.currentDateForCalendarPage
import java.util.Calendar

@Composable
fun PagerMonth(
    modifier: Modifier = Modifier,
    currentDate: Calendar
) {
    Column(modifier = modifier) {
        DaysOfWeek(modifier = Modifier.fillMaxWidth())

        Spacer(modifier = Modifier.height(12.dp))

        MonthContent(
            modifier = Modifier
                .testTag(TAG_MONTH_PAGE + currentDate)
                .fillMaxSize(),
            date = currentDateForCalendarPage(currentDate)
        )
    }
}