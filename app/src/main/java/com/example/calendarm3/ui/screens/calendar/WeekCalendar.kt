package com.example.calendarm3.ui.screens.calendar

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.example.calendarm3.R
import com.example.calendarm3.data.CalendarEvent
import com.example.calendarm3.utils.Tags.TAG_DAYS_OF_WEEK
import com.example.calendarm3.utils.Tags.TAG_WEEK_ROW
import com.example.calendarm3.utils.WeekDay
import java.util.Calendar

@Composable
fun WeekCalendar(
    modifier: Modifier = Modifier,
    calendarWeek: Calendar,
    events: List<CalendarEvent>
) {
    Row(
        modifier = modifier.testTag(TAG_WEEK_ROW),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        (0 until 7).forEach { position ->
            val dateForMonthCell = calendarWeek.clone() as Calendar

            dateForMonthCell.add(Calendar.DAY_OF_WEEK, position)

            WeekDay(date = dateForMonthCell)
        }
    }
}

@Composable
fun DaysOfWeek(modifier: Modifier = Modifier) {
    Row(
        modifier = modifier.testTag(TAG_DAYS_OF_WEEK),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        stringArrayResource(id = R.array.days_of_week).forEach {
            Text(
                modifier = Modifier.weight(1f),
                text = it,
                textAlign = TextAlign.Center,
                fontSize = 12.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}