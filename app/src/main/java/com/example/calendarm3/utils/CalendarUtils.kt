package com.example.calendarm3.utils

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.calendarm3.ui.screens.calendar.WeekCalendar
import com.example.calendarm3.utils.EventFactory.events
import com.example.calendarm3.utils.Tags.TAG_MONTH
import java.util.Calendar

@Composable
fun RowScope.WeekDay(
    modifier: Modifier = Modifier,
    date: Calendar
) {
    Column(
        modifier
            .weight(1f)
            .fillMaxHeight()
            .drawBehind {
                val strokeWidth = 1 * density
                drawLine(
                    Color.LightGray,
                    Offset(size.width, 0f),
                    Offset(size.width, size.height),
                    strokeWidth
                )
            },
        horizontalAlignment = Alignment.End
    ) {
        Divider(thickness = 1.dp)

        val isToday = isSameDate(Calendar.getInstance(), date)

        Text(
            modifier = Modifier.dateBackground(isToday),
            text = date.get(Calendar.DAY_OF_MONTH).toString(),
            fontSize = 12.sp,
            textAlign = TextAlign.End,
            color = if (isToday) Color.White else MaterialTheme.colorScheme.onSurface
        )

        Spacer(modifier = Modifier.height(2.dp))

        EventStack(
            modifier = Modifier.fillMaxWidth(),
            events = events,
            date = date
        )
    }
}

@Composable
fun MonthContent(
    modifier: Modifier = Modifier,
    date: Calendar
) {
    Column(
        modifier = modifier.testTag(TAG_MONTH),
        verticalArrangement = Arrangement.SpaceEvenly
    ) {
        (0 until 6).forEach { position ->
            val dateForMonthCell = (date.clone() as Calendar).apply {
                add(Calendar.WEEK_OF_YEAR, position)
            }
            WeekCalendar(
                modifier = Modifier
                    .testTag(
                        dateForMonthCell
                            .get(Calendar.WEEK_OF_YEAR)
                            .toString()
                    )
                    .fillMaxWidth()
                    .weight(1f),
                calendarWeek = dateForMonthCell,
                events = events
            )
        }
    }
}

@SuppressLint("ModifierFactoryUnreferencedReceiver")
fun Modifier.dateBackground(isToday: Boolean): Modifier {
    return if (isToday) {
        this
            .padding(4.dp)
            .background(Color.Blue, CircleShape)
            .padding(4.dp)
    } else {
        this.padding(8.dp)
    }
}