package com.example.calendarm3.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreHoriz
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.calendarm3.R
import com.example.calendarm3.data.CalendarEvent
import com.example.calendarm3.data.EventType
import com.example.calendarm3.utils.Tags.TAG_EVENTS_STACK
import java.util.Calendar

@Composable
fun EventStack(
    modifier: Modifier = Modifier,
    events: List<CalendarEvent>,
    date: Calendar
) {
    if (events.isNotEmpty()) {
        Layout(
            modifier = modifier.testTag(TAG_EVENTS_STACK),
            content = {
                events.forEach { event ->
                    val eventType = eventType(
                        date,
                        event.startDate, event.endDate
                    )
                    if (eventType != EventType.NONE) {
                        EventBox(event.color, eventType)
                    }
                }
                Icon(
                    imageVector = Icons.Default.MoreHoriz,
                    contentDescription = stringResource(id = R.string.cd_view_more_events)
                )
            }
        ) { measurables, constraints ->
            val placeables = measurables.map { measurable ->
                measurable.measure(constraints)
            }

            layout(constraints.maxWidth, constraints.maxHeight) {
                var yPosition = 0

                val showMore = placeables.subList(0, placeables.count() - 1).sumOf {
                    it.height
                } > constraints.maxHeight

                val availableHeight = constraints.maxHeight - placeables.last().height

                placeables.subList(0, placeables.count() - 1).forEach { placeable ->
                    if (!showMore || (yPosition + placeable.height < availableHeight)) {
                        placeable.placeRelative(x = 0, y = yPosition)
                        yPosition += placeable.height
                    }
                }
                if (showMore) {
                    val more = placeables.last()
                    more.placeRelative(x = constraints.maxWidth - more.width - 5, y = yPosition)
                }
            }
        }
    }
}

@Composable
fun EventBox(color: Color, eventType: EventType) {
    Box(
        Modifier
            .padding(bottom = 2.dp)
            .eventBackground(color, eventType)
            .height(5.dp)
            .fillMaxWidth()
    )
}

fun Modifier.eventBackground(
    color: Color,
    eventType: EventType
): Modifier {
    return when (eventType) {
        EventType.START -> {
            this
                .padding(start = 2.dp)
                .background(
                    color, RoundedCornerShape(
                        2.dp, 0.dp, 0.dp, 2.dp
                    )
                )
        }
        EventType.END -> {
            this
                .padding(end = 2.dp)
                .background(
                    color, RoundedCornerShape(
                        0.dp, 2.dp, 2.dp, 0.dp
                    )
                )
        }
        EventType.SINGLE_DAY -> {
            this
                .padding(horizontal = 2.dp)
                .background(
                    color, RoundedCornerShape(
                        2.dp, 2.dp, 2.dp, 2.dp
                    )
                )
        }
        else -> {
            this.background(
                color, RoundedCornerShape(
                    0.dp, 0.dp, 0.dp, 0.dp
                )
            )
        }
    }
}

fun eventsForCurrentWeek(
    date: Calendar,
    events: List<CalendarEvent>
): List<CalendarEvent> {
    return events
        .filter { withinCurrentWeek(date, it.startDate, it.endDate) }
}

fun eventType(
    currentDate: Calendar,
    eventStart: Calendar,
    eventEnd: Calendar
): EventType {
    return when {
        isSameDate(currentDate, eventStart) && isSameDate(currentDate, eventEnd)
        -> EventType.SINGLE_DAY
        isSameDate(currentDate, eventStart) -> EventType.START
        isSameDate(currentDate, eventEnd) -> EventType.END
        isBetweenDates(currentDate, eventStart, eventEnd) -> EventType.MIDDLE
        else -> EventType.NONE
    }
}