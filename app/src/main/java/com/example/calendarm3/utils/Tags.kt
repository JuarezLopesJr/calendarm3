package com.example.calendarm3.utils

object Tags {
    const val TAG_DAYS_OF_WEEK = "days_of_week"
    const val TAG_WEEK_ROW = "week_row"
    const val TAG_EVENTS_STACK = "events_stack"
    const val TAG_MONTH = "month"
    const val TAG_HEADER = "header"
    const val TAG_MONTH_PAGE = "month_page_"
}