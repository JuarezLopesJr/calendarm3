package com.example.calendarm3.utils

import androidx.compose.ui.graphics.Color
import com.example.calendarm3.data.CalendarEvent
import java.util.Calendar
import java.util.UUID

object TestDataFactory {
    fun randomString() = UUID.randomUUID().toString()

    fun events(
        count: Int,
        date: Calendar
    ): List<CalendarEvent> {
        return (0 until count).map {
            CalendarEvent(
                Color.Red,
                date,
                date
            )
        }
    }
}